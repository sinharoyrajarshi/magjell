       !----------------------------------------------------------------------------------------------
       ! 
       !  THIS creates [rhots(t)-rhogs] files for a given time step
       !  which later is to be used for FOURiER Tranform. 
       ! 
       !----------------------------------------------------------------------------------------------
       program induced_density
       implicit none
       integer n_max,in,nx,ny,nz,tstp,i,j,k,ntop,n
       integer start,interval,end
       double precision x0,y0,z0,spacing,s
       character (len=90) :: flnm, slash
       character*50 :: filename
       character(1000) fileplace, f1, f2
       character(100) path_dir
       CHARACTER(LEN=1200) :: longstring
       character*24 gbg
       parameter (ntop=200)
       DOUBLE PRECISION, allocatable :: rhot(:,:,:)
       DOUBLE PRECISION, allocatable :: rhogs(:,:,:)

       !----------------------------------------------------------------------------------------------
       !  READING ground-state densities from the file "...../.../....../static/density.dx" and
       !  then storing in rhogs(x,y,z).
       !  Here we also read all the different grid information from the file
       !  And then we write the (grond-state-density*cross-section) as a function of axis 
       !  perpendicula to the cross-section
       !----------------------------------------------------------------------------------------------
       write(*,*) 'Give the path for the directory where /static and /td.xxxxxx are &
            stored ended by trailing slash'
       read(*,'(a)') path_dir
       !write(*,*) path_dir
       WRITE(f1,'(a)') trim(path_dir)//"static/density.dx"
       write(*,*) 'We are going for ground state density @ ',trim(f1)
       open(11,file=trim(f1),status='old')
       read(11,*) gbg,gbg,gbg,gbg,gbg,nx,ny,nz
       write(*,*) 'The number of x-points =',nx
       write(*,*) 'The number of y-points =',ny
       write(*,*) 'The number of z-points =',nz
       read(11,*) gbg,x0,y0,z0
       read(11,*) gbg, spacing
       read(11,*) gbg
       read(11,*) gbg
       read(11,*) gbg
       read(11,*) gbg
       !-----------------------------------Allocating_arrays--------------
       allocate (rhogs(nx+1,ny+1,nz+1))
       allocate (rhot(nx+1,ny+1,nz+1))
       !------------------------------------------------------------------
       do i=1,nx
          do j=1,ny
             do k=1,nz
                read(11,*) rhogs(i,j,k)
             end do
          end do
       end do
       close(11)

       !===========================================================================================
       !
       !                               LOOP FOR ALL THE TIMESTEP DIRECTORIES
       !
       !*******************************************************************************************
       write(*,*) 'Starting time step:'
       read(*,*) start
       write(*,*) 'Ending time step:'
       read(*,*) end
       write(*,*) 'Interva between time steps:'
       read(*,*) interval       
       do tstp=start,end,interval
          !-------------------------------------------------------------------------------------------!
          !                                                                                           !
          !                              TD INDUCED DENSITY at a SINGLE timestep                      !
          !                                                                                           !
          !-------------------------------------------------------------------------------------------!
          !!!!!!!!!! write(*,*) 'Give the TIME-STEP for which induced density is needed'
          !!!!!!!!!! read(*,*) tstp
          !!!!!!!!!! write(*,*) tstp
          write(flnm,'(I7.7,a)') tstp
          WRITE(slash,'(a)')"/"
          WRITE(f2,'(a)') trim(path_dir)//"output_iter/"//"td."//trim(flnm)//trim(slash)
          !write(*,*) trim(f2)
          write(*,*) 'We are now going for ',trim(f2),'density.dx'
          open(unit=379, file=trim(f2)//"density.dx" ,status='old')
          open(unit=45, file=trim(path_dir)//"IND_DENS/"//trim(flnm)//"_density_induced.dx",status='unknown')
       
       !----reading header from td.xxxxxxx/density.dx and writing to o/p file------------
       read(379, '(a)', end=10) longstring
       write(45,'(a)') trim(longstring)
       read(379,'(a)', end=10) longstring
       write(45,'(a)') trim(longstring)
       read(379,'(a)', end=10) longstring
       write(45,'(a)')  trim(longstring)
       read(379,'(a)', end=10) longstring
       write(45,'(a)')  trim(longstring)
       read(379,'(a)', end=10) longstring
       write(45,'(a)')  trim(longstring)
       read(379,'(a)', end=10) longstring
       write(45,'(a)')  trim(longstring)
       read(379,'(a)', end=10) longstring
       write(45,'(a)')  trim(longstring)
       
       !------------reading td.xxxxxxx/density.dx data and writing to o/p file------------
       do i=1,nx
          do j=1,ny
             do k=1,nz
                read(379,*) rhot(i,j,k)
                write(45,*) rhot(i,j,k)-rhogs(i,j,k)
             end do
          end do
       end do
       
       !----reading 'tail' from td.xxxxxxx/density.dx and writing to o/p file------------
       read(379,'(a)', end=10) longstring
       write(45,'(a)')  trim(longstring)
       read(379,'(a)', end=10) longstring
       write(45,'(a)')  trim(longstring)
       read(379,'(a)', end=10) longstring
       write(45,'(a)')  trim(longstring)
       read(379,'(a)', end=10) longstring
       write(45,'(a)')  trim(longstring)
       read(379,'(a)', end=10) longstring
       write(45,'(a)')  trim(longstring)       

       10 continue
       close(379)
       close(45)
       write(*,*) 'Induced density for time-step',tstp,'is created @',trim(f2),'density_induced.dx'
       
       !-------------------------------------------------------------------------------------------
       !                              TD INDUCED DENSITY at a SINGLE timestep                      !
       !-------------------------------------------------------------------------------------------
     end do
     !*********************************************************************************************
     !                           END OF LOOP FOR ALL THE TIMESTEP DIRECTORIES
     !=============================================================================================
       
     end program induced_density
     




































































































