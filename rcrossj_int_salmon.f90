program rcrossj_int
  implicit none
  integer*4 now(3), today(3)
  double precision mx, my, mz, angstrom_in_au, jellcen_x, jellcen_y, jellcen_z 
  integer n_max,tstp,max_tstp,tstp_intrvl,i,j,k,n,AllocateStatus,n_w,prmtr,omega,line
  integer xx, yy, zz, ilspr, iw1, iw2, iw3
  double precision spacing,dtm,tm(0:10000),tm_max, t_cut,w_los,w_max,AVG,dw
  character (len=90) :: flnm, slash, filename
  character(100) path, folder, folder1, folder2, folder3, makedirectory*40
  character(1000) f1, f2
  character*24 string
  double precision, DIMENSION(:, :, :), ALLOCATABLE :: rhogs, jx, jy, jz, rxj_x, rxj_y,rxj_z
  double precision, DIMENSION(:, :), ALLOCATABLE :: Sxy
  double precision, DIMENSION(:), ALLOCATABLE :: x, y, z, func, Sx, func_y

  integer :: NATOMS, NX, NY, NZ, GEOM1, I1, I2, I3
  double precision X0, Y0, Z0, dx, dy, dz, CHRG1, zeors
  double precision GEOM1X, GEOM1Y, GEOM1Z
  double precision autoarm
  CHARACTER(len=100) :: gbg, stepname



  angstrom_in_au = 1.889726 ! 1 Angstrom = 1.889726 atomic units
  
  open(21,file="input_rcrossj_int_salmon",status='old')
  write(*,*) ' '
  write(*,*) ' '
  read(21,'(a)') f1
  write(*,*) 'The path for the cube files is ', trim(f1)
  write(*,*) ' '
  read(21,*) jellcen_x, jellcen_y, jellcen_z
  write(*,*) 'Center of jellium : (x, y, z) =', jellcen_x, jellcen_y, jellcen_z
  write(*,*) ' '
  write(*,*) 'GOING FOR: '
  write(*,*) trim(f1),'dns_000000.cube'
  write(*,*) ' '
  open(11,file=trim(f1)//"dns_000000.cube",status='old')
 ! read(11,*) gbg,gbg,gbg,gbg,gbg,nx,ny,nz
 ! read(11,*) gbg,x0,y0,z0
 ! read(11,*) gbg, spacing
 ! read(11,*) gbg
 ! read(11,*) gbg
 ! read(11,*) gbg
 ! read(11,*) gbg
  read(11,*) gbg
  write(*,*) gbg
  read(11,*) gbg
  write(*,*) gbg
  read(11,*) NATOMS, X0, Y0, Z0
  write(*,*) NATOMS, X0, Y0, Z0
  read(11,*) NX, dx, zeors, zeors
  read(11,*) NY, zeors, dy, zeors
  read(11,*) NZ, zeors, zeors, dz
  write(*,*) NX, dx, zeors, zeors
  write(*,*) NY, zeors, dy, zeors
  write(*,*) NZ, zeors, zeors, dz
  read(11,*) GEOM1, CHRG1, GEOM1X, GEOM1Y, GEOM1Z
  write(*,*) GEOM1, CHRG1, GEOM1X, GEOM1Y, GEOM1Z
  close(11)

  write(*,*) 'No. of points in each .dx (nx,ny,nz) = (',nx,ny,nz, ')'
  write(*,*) ' '
  write(*,*) 'Origin is set at (x0,y0,z0) atomic units'
  write(*,*) 'x0 =', x0
  write(*,*) 'y0 =', y0
  write(*,*) 'z0 =', z0
  write(*,*) 'xSpacing used', dx, '[in atomic units] =', dx/angstrom_in_au, 'Armstrong'
  write(*,*) 'ySpacing used', dy, '[in atomic units] =', dy/angstrom_in_au, 'Armstrong'
  write(*,*) 'zSpacing used', dz, '[in atomic units] =', dz/angstrom_in_au, 'Armstrong'
  write(*,*) ' '
  dx = dx/angstrom_in_au
  dy = dy/angstrom_in_au
  dz = dz/angstrom_in_au
  
!  open(13,file=trim(f1)//"coords.txt",status='unknown')
!  ALLOCATE ( rhogs(nx, ny, nz), STAT = AllocateStatus)
!  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
!  do i=1,nx
!     do j=1,ny
!        do k=1,nz
!           read(11,*) rhogs(i,j,k)
!           write(13,*) (x0+(i-1)*spacing), (y0+(j-1)*spacing), (z0+(k-1)*spacing)
!        end do
!     end do
!  end do
!  close(11)
!  close(13)
  !---------------------------------------------------------------------------------------------
  !              Here we take all the input information we need to run this program
  !---------------------------------------------------------------------------------------------
  read(21,*) max_tstp
  write(*,*) 'The maximum nuber of time step to be considered = ', max_tstp
  read(21,*) tstp_intrvl
  write(*,*) 'The step interval used in SALMON td run to generate "data_XXXXXX.cube" files = ', tstp_intrvl
  prmtr=tstp_intrvl
  read(21,*) dtm
  write(*,*) 'TDTimeStep in femtosecond units used in octopus td run = dt = ', dtm
  n_max=max_tstp/tstp_intrvl
  write(*,*) 'Total number of td-files to be treated =', n_max
  write(*,*) ' '
  

  do n=0,n_max
     tm(n)=n*dtm*prmtr                 ! 'i.e. we take time in atomic units'
  end do
  tm_max=tm(n_max)  !
  write(*,*) 'Total TIME of evolution in femtosecond is',tm_max
  close(21)


  open(35,file=trim(f1)//"magmom_integrated-rxj_salmon.dat",status='unknown')
  write(35,*) "### tstp, tstp*tstp_intrvl*dtm, mx, my, mz !!!! Time in femtosecond"
  !write(35,*) "### tstp, tstp*tstp_intrvl*dtm/27.212, mx, my, mz ! Time in a.u."

  spacing = dx
  write(*,*) 'The spacing of the grid in Armstrong =', spacing
  write(*,*) ' '
  ALLOCATE ( x(nx), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
  ALLOCATE ( y(ny), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
  ALLOCATE ( z(nz), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
  do i=1,nx
     x(i) = (i-1)*dx - jellcen_x
  enddo
  do j=1,ny
     y(j) = (j-1)*dy - jellcen_y
  enddo
  do k=1,nz
     z(k) = (k-1)*dz - jellcen_z
  enddo

  write(*,*) ' '
  write(*,*) 'For calculation of particle-centered angular momentum, '
  write(*,*) 'the MODIFIED grid points [Armstrong] on the cartesian axes are as follows :'
  do i=1,nx
     write(*,*) i, x(i), y(i), z(i)
  enddo   
  write(*,*) ' '
  write(*,*) ' '
     
  
  do tstp=1,n_max
     !!!! write(*,*) tstp*prmtr
     write(flnm,'(I6.6,a)') tstp*prmtr
     write(*,*) 'We are now going for time-dep. ******.cube files for step ', flnm
     WRITE(slash,'(a)')"/"
 
     mx = 0.0
     my = 0.0
     mz = 0.0
     
     open(15,file=trim(f1)//"je_micro_x_"//trim(flnm)//".cube",status='old')
     open(16,file=trim(f1)//"je_micro_y_"//trim(flnm)//".cube",status='old')
     open(17,file=trim(f1)//"je_micro_z_"//trim(flnm)//".cube",status='old')

     ALLOCATE ( jx(nx, ny, nz), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
     ALLOCATE ( jy(nx, ny, nz), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
     ALLOCATE ( jz(nx, ny, nz), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
 
     ALLOCATE ( rxj_x(nx, ny, nz), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
     ALLOCATE ( rxj_y(nx, ny, nz), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
     ALLOCATE ( rxj_z(nx, ny, nz), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
     do line=1,7
        read(15,*) gbg
        read(16,*) gbg
        read(17,*) gbg
     enddo
     do i=1,nx
        do j=1,ny
           !!do k=1,nz
              read(15,*) ( jx(i,j,k), k=1,nz )
              read(16,*) ( jy(i,j,k), k=1,nz )
              read(17,*) ( jz(i,j,k), k=1,nz )
           !!end do
        end do
     end do
     do i=1,nx
        do j=1,ny
           do k=1,nz
              rxj_x(i,j,k) = y(j)*jz(i,j,k) - z(k)*jy(i,j,k)
              rxj_y(i,j,k) = z(k)*jx(i,j,k) - x(i)*jz(i,j,k)
              rxj_z(i,j,k) = x(i)*jy(i,j,k) - y(j)*jx(i,j,k)
           end do
        end do
     end do
     close(15)
     close(16)
     close(17)
     

!!!!!MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM     
!!!!!             Writing rXj in dx format
!!!!!MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM     

     open(18,file=trim(f1)//"rxj_x_"//trim(flnm)//".dx",status='unknown')
     open(19,file=trim(f1)//"rxj_y_"//trim(flnm)//".dx",status='unknown')
     open(20,file=trim(f1)//"rxj_z_"//trim(flnm)//".dx",status='unknown')     
     write(18,*) 'object 1 class gridpositions counts', nx, ny, nz
     write(18,*) ' origin',    x0,          y0,          z0
     write(18,*) ' delta',     spacing,     0.000000,    0.000000
     write(18,*) ' delta',     0.000000,    spacing,     0.000000
     write(18,*) ' delta',     0.000000,    0.000000,    spacing
     write(18,*) 'object 2 class gridconnections counts', nx, ny, nz
     write(18,*) 'object 3 class array type float rank 0 items', nx*ny*nz,'         data follows'
     write(19,*) 'object 1 class gridpositions counts', nx, ny, nz
     write(19,*) ' origin',    x0,          y0,          z0
     write(19,*) ' delta',     spacing,     0.000000,    0.000000
     write(19,*) ' delta',     0.000000,    spacing,     0.000000
     write(19,*) ' delta',     0.000000,    0.000000,    spacing
     write(19,*) 'object 2 class gridconnections counts', nx, ny, nz
     write(19,*) 'object 3 class array type float rank 0 items', nx*ny*nz,'         data follows'
     write(20,*) 'object 1 class gridpositions counts', nx, ny, nz
     write(20,*) ' origin',    x0,          y0,          z0
     write(20,*) ' delta',     spacing,     0.000000,    0.000000
     write(20,*) ' delta',     0.000000,    spacing,     0.000000
     write(20,*) ' delta',     0.000000,    0.000000,    spacing
     write(20,*) 'object 2 class gridconnections counts', nx, ny, nz
     write(20,*) 'object 3 class array type float rank 0 items', nx*ny*nz,'         data follows'
     do i=1,nx
        do j=1,ny
           do k=1,nz
              write(18,*) rxj_x(i,j,k)
              write(19,*) rxj_y(i,j,k)
              write(20,*) rxj_z(i,j,k)
           end do
        end do
     end do
     write(18,*) 'object "regular positions regular connections" class field'
     write(18,*) ' component "positions" value 1'
     write(18,*) ' component "connections" value 2'
     write(18,*) ' component "data" value 3'
     write(18,*) 'end'
     write(19,*) 'object "regular positions regular connections" class field'
     write(19,*) ' component "positions" value 1'
     write(19,*) ' component "connections" value 2'
     write(19,*) ' component "data" value 3'
     write(19,*) 'end'
     write(20,*) 'object "regular positions regular connections" class field'
     write(20,*) ' component "positions" value 1'
     write(20,*) ' component "connections" value 2'
     write(20,*) ' component "data" value 3'
     write(20,*) 'end'
     close(18)
     close(19)
     close(20)

!!!!!MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM     

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!        
     ALLOCATE ( func(nz), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
     ALLOCATE ( Sxy(nx, ny), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
     ALLOCATE ( func_y(ny), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
     ALLOCATE ( Sx(nx), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
     

     call VOL_INTEGRATION(rxj_x,Sxy,func,func_y,Sx,nx,ny,nz,mx,spacing)
     call VOL_INTEGRATION(rxj_y,Sxy,func,func_y,Sx,nx,ny,nz,my,spacing)
     call VOL_INTEGRATION(rxj_z,Sxy,func,func_y,Sx,nx,ny,nz,mz,spacing)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     IF( ALLOCATED(func) ) DEALLOCATE (func)
     IF( ALLOCATED(func_y) ) DEALLOCATE (func_y)
     IF( ALLOCATED(Sxy) ) DEALLOCATE (Sxy)
     IF( ALLOCATED(Sx) ) DEALLOCATE (Sx)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     
     !write(*,*) mx
     !write(*,*) my
     !write(*,*) mz
     
     write(35,*) tstp, tstp*tstp_intrvl*dtm, mx, my, mz ! Time in hbar/eV
     !write(35,*) tstp, tstp*tstp_intrvl*dtm/27.212, mx, my, mz ! Time in a.u.
     
     IF( ALLOCATED(jx) ) DEALLOCATE (jx)
     IF( ALLOCATED(jy) ) DEALLOCATE (jy)
     IF( ALLOCATED(jz) ) DEALLOCATE (jz)
     IF( ALLOCATED(rxj_x) ) DEALLOCATE (rxj_x)
     IF( ALLOCATED(rxj_y) ) DEALLOCATE (rxj_y)
     IF( ALLOCATED(rxj_z) ) DEALLOCATE (rxj_z)
     
  enddo
  close(35)
    
end program rcrossj_int


!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

Subroutine trapizoidal(F,integral,n,deltax)
  double precision integral
  double precision deltax
  double precision, DIMENSION(n) :: F
  integer n, i
  integral = F(1)+F(n)
  do i=2, n-2, 1
     integral = integral + 2*F(i)
  end do
  integral = integral*0.5*deltax
return
end subroutine trapizoidal

!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

Subroutine VOL_INTEGRATION(integrand,Sxy,func,func_y,Sx,nx,ny,nz,integral,grid_spacing)
  
  double precision,  DIMENSION(nx,ny,nz) :: integrand
  double precision,  DIMENSION(nx,ny) :: Sxy
  double precision,  DIMENSION(nz) :: func
  double precision,  DIMENSION(ny) :: func_y
  double precision,  DIMENSION(nx) :: Sx
  integer i,j,k,nx,ny,nz
  double precision integral, justsum
  double precision grid_spacing

!  justsum = 0.d0
!  do i=1,nx
!     do j=1,ny
!        do k=1,nz
!           justsum = justsum + integrand(i,j,k)
!        enddo
!     end do
!  end do
!  write(*,*) justsum

  do i=1,nx
     do j=1,ny
        do k=1,nz
           func(k) = integrand(i,j,k)   !!!! The data in 3d grid enters here.
        enddo
        call trapizoidal(func,integral,nz,grid_spacing)
        Sxy(i,j) = integral
        !write(*,*) i, j, Sxy(i,j)
     end do
  end do  
  !!=========================================================================================   
  !! If you want to see S(x,y), the z-integrated integrand(x,y,z) can be written at this step 
  !!=========================================================================================   

  do i=1,nx
     do j=1,ny
        func_y(j) = Sxy(i,j)
     end do
     call trapizoidal(func_y,integral,ny,grid_spacing)
     Sx(i) = integral
     !write(*,*) i, Sx(i)
  end do  
  !!=============================================================================   
  !! If you want to see S(x), the y-integrated S(x,y) can be written at this step
  !!=============================================================================  
  
  call trapizoidal(Sx,integral,nx,grid_spacing)
  return
  
end subroutine VOL_INTEGRATION
      
!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@       
