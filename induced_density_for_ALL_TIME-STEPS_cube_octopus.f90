program inddens
  implicit none
  integer*4 now(3), today(3)
  double precision angstrom_in_au, cen_x, cen_y, cen_z 
  integer n_max,nx,ny,nz,tstp,max_tstp,tstp_intrvl,i,j,k,n,AllocateStatus,n_w,prmtr,omega,line
  double precision spacing,dtm,tm(0:10000),tm_max, t_cut,w_los,w_max,AVG,dw
  character (len=90) :: flnm, slash, filename
  character(100) path, folder, folder1, folder2, folder3, makedirectory*40
  character(1000) f1, f2
  character*24 string
  double precision, DIMENSION(:, :, :), ALLOCATABLE :: rhogs, rhotd, rhoind

  integer, DIMENSION(:), ALLOCATABLE :: GEOM1
  double precision, DIMENSION(:), ALLOCATABLE :: CHRG1, GEOM1X, GEOM1Y, GEOM1Z
  integer :: NATOMS
  double precision x0, y0, z0, dx, dy, dz, zeors
  double precision autoarm
  CHARACTER(len=100) :: gbg, stepname


  angstrom_in_au = 1.889726 ! 1 Angstrom = 1.889726 atomic units

  open(21,file="input_induced_density_octopus",status='old')
  write(*,*) ' '
  read(21,'(a)') f1
  write(*,*) 'The path for the static/ and the output_iter/ is ', trim(f1)
  write(*,*) ' '
  read(21,*) cen_x, cen_y, cen_z
  write(*,*) 'Center of the structure : (x, y, z) =', cen_x, cen_y, cen_z
  write(*,*) ' '
   write(*,*) 'GOING FOR: '
  write(*,*) trim(f1),'static/density.cube'
  write(*,*) ' '
  open(11,file=trim(f1)//"static/density.cube",status='old')

  read(11,*) gbg
  write(*,*) gbg
  read(11,*) gbg
  write(*,*) gbg
  read(11,*) NATOMS, x0,y0,z0
  write(*,*) NATOMS, x0,y0,z0
  read(11,*) nx, dx, zeors, zeors
  read(11,*) ny, zeors, dy, zeors
  read(11,*) nz, zeors, zeors, dz
  write(*,*) nx, dx, zeors, zeors
  write(*,*) ny, zeors, dy, zeors
  write(*,*) nz, zeors, zeors, dz

  ALLOCATE ( GEOM1(NATOMS), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
  ALLOCATE ( CHRG1(NATOMS), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
  ALLOCATE ( GEOM1X(NATOMS), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
  ALLOCATE ( GEOM1Y(NATOMS), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
  ALLOCATE ( GEOM1Z(NATOMS), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"

  DO i=1,NATOMS
     read(11,*) GEOM1(i), CHRG1(i), GEOM1X(i), GEOM1Y(i), GEOM1Z(i)
     write(*,'(I4,E13.5,3F13.5)') GEOM1(i), CHRG1(i), GEOM1X(i), GEOM1Y(i), GEOM1Z(i)
  ENDDO

  write(*,*) 'No. of points in each .cube (nx,ny,nz) = (',nx,ny,nz, ')'
  write(*,*) ' '
  write(*,*) 'Origin is set at (x0,y0,z0) atomic units'
  write(*,*) 'x0 =', x0
  write(*,*) 'y0 =', y0
  write(*,*) 'z0 =', z0
  write(*,*) 'xSpacing used', dx, '[in atomic units]' !=', dx/angstrom_in_au, 'Armstrong'
  write(*,*) 'ySpacing used', dy, '[in atomic units]' !=', dy/angstrom_in_au, 'Armstrong'
  write(*,*) 'zSpacing used', dz, '[in atomic units]' !=', dz/angstrom_in_au, 'Armstrong'
  write(*,*) ' '
  IF(dx==dy.AND.dy==dz)THEN
     spacing=dx
  ELSE
     write(*,*) ' '
     write(*,*) 'Spacings are different in x, y, and z directions!'
     write(*,*) 'This program will not work!'
     write(*,*) ' '
  ENDIF
!  dx = dx/angstrom_in_au
!  dy = dy/angstrom_in_au
!  dz = dz/angstrom_in_au

  ALLOCATE ( rhogs(nx, ny, nz), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
  do i=1,nx
     do j=1,ny
        !!do k=1,nz
        read(11,*) ( rhogs(i,j,k), k=1,nz )
        !!end do
     end do
  end do
  
  close(11)

  !---------------------------------------------------------------------------------------------
  !              Here we take all the input information we need to run this program
  !---------------------------------------------------------------------------------------------
  read(21,*) max_tstp
  write(*,*) 'The maximum nuber of time step to be considered = ', max_tstp
  read(21,*) tstp_intrvl
  write(*,*) 'The step interval used in octopus td run to generate td.0XXXXXX/ = ', tstp_intrvl
  prmtr=tstp_intrvl
  read(21,*) dtm
  write(*,*) 'TDTimeStep in eV-A units used in octopus td run = dt = ', dtm
  write(*,*) ' '
  n_max=max_tstp/tstp_intrvl
  write(*,*) 'The number of TD steps =',n_max
  

  do n=0,n_max
     tm(n)=n*dtm*prmtr                 ! 'i.e. we take time in atomic units'
  end do
  tm_max=tm(n_max)  !
  write(*,*) 'Total TIME in eV-A units used in octopus td run is',tm_max
  close(21)

  do tstp=0,n_max
     !!!! write(*,*) tstp*prmtr
     write(flnm,'(I7.7,a)') tstp*prmtr
     write(*,*) 'We are now going for density.cube files in td.',flnm
     WRITE(slash,'(a)')"/"
     
     open(15,file=trim(f1)//"output_iter/td."//trim(flnm)//trim(slash)//"density.cube",status='old')

     ALLOCATE ( rhotd(nx, ny, nz), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
     ALLOCATE ( rhoind(nx, ny, nz), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"

     do line=1,NATOMS+6
        read(15,*) gbg
     enddo
     do i=1,nx
        do j=1,ny
           !!do k=1,nz
              read(15,*) ( rhotd(i,j,k), k=1,nz )
           !!end do
        end do
     end do
     close(15)
     

!!!!!MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM     
!!!!!             Calculating induced density 
!!!!!MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM     
     do i=1,nx
        do j=1,ny
           do k=1,nz
              rhoind(i,j,k) = rhotd(i,j,k) - rhogs(i,j,k)
           end do
        end do
     end do
!!!!!MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM     

     
!!!!!MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM     
!!!!!             Writing induced density in cube format
!!!!!MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM     

     open(18,file=trim(f1)//"output_iter/td."//trim(flnm)//trim(slash)//"ind-dens.cube",status='unknown')
     write(18,*) 'Generated by induced_density_for_ALL_TIME-STEPS_cube_octopus.f90 code'
     write(18,*) 'Generated by induced_density_for_ALL_TIME-STEPS_cube_octopus.f90 code'
     write(18,'(I4,3E13.5)') NATOMS, x0,y0,z0
     write(18,'(I4,3E13.5)') nx, dx, zeors, zeors
     write(18,'(I4,3E13.5)') ny, zeors, dy, zeors
     write(18,'(I4,3E13.5)') nz, zeors, zeors, dz
     DO i=1,NATOMS
        write(18,'(I4,E13.5,3E13.5)') GEOM1(i), CHRG1(i), GEOM1X(i), GEOM1Y(i), GEOM1Z(i)
     ENDDO
     do i=1,nx
        do j=1,ny
           !do k=1,nz
              write(18,'(6E13.5)') (rhoind(i,j,k),k=1,nz)
           !end do
        end do
     end do
     close(18)

!!!!!MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM     
    
     
     IF( ALLOCATED(rhotd) ) DEALLOCATE (rhotd)
     IF( ALLOCATED(rhoind) ) DEALLOCATE (rhoind)
     
  enddo
    
end program inddens
