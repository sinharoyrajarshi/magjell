       !----------------------------------------------------------------------------------------------
       ! 
       !  THIS creates rhotd_diff(t) = [rhots(t)-rhogs] files at a desired indexed point of space
       !  which later is to be used for FOURiER Tranform. 
       ! 
       !----------------------------------------------------------------------------------------------
       program induced_density
       implicit none
       integer n_max,in,nx,ny,nz,tstp,i,j,k,ntop,n,omega,n_w,AllocateStatus
       integer start,end,interval,totsteps,reso
       double precision x0,y0,z0,spacing,s,w_max,dw
       real x
       character (len=90) :: flnm, slash
       character*50 :: filename, strstart,strend
       character(1000) fileplace, f1, f2
       character(100) path
       character*24 gbg, string
       parameter (ntop=200)
!       DOUBLE PRECISION, allocatable :: rhot(:,:,:), tdrho_z(:), w(:)
!       DOUBLE PRECISION, allocatable :: rhogs(:,:,:)
       REAL, allocatable :: jx(:,:,:,:), jy(:,:,:,:), jz(:,:,:,:)
       REAL, allocatable :: sumjx(:,:,:), sumjy(:,:,:),  sumjz(:,:,:)
       
     
       write(*,*) ' Give the starting time step'
       read(*,*) start
       write(strstart,'(I4.4,a)') start
       write(*,*) ' Give the ending time step'
       read(*,*) end
       write(strend,'(I4.4,a)') end
       write(*,*) ' Give the interval time step'
       read(*,*) interval
       write(*,*) ' Give the resolution of grid'
       write(*,*) ' 1 for all point, n for every nth points'
       read(*,*) reso      

       totsteps=int((end-start)/interval)+1
       write(*,*) ' Give the ending interval timetotal number of steps', totsteps

       write(f1,'(I7.7,a)') start
       open(unit=34, file="output_iter/td."//trim(f1)//"/current-x.dx",status='old')
       read(34,*) gbg,gbg,gbg,gbg,gbg,nx,ny,nz
       read(34,*) gbg,x0,y0,z0
       read(34,*) gbg, spacing
       read(34,*) gbg
       read(34,*) gbg
       read(34,*) gbg
       read(34,*) gbg
       close(34)
       
!!!!-----------------------------------------------------------------------------         
       ALLOCATE ( jx(totsteps,nx,ny,nz), STAT = AllocateStatus)
       IF (AllocateStatus /= 0) STOP "*** Not enough memory FOR jx ***"
       write(*,*) 'AllocateStatus for jx=',AllocateStatus
       write(*,*)'(nx*ny*nz) = (',totsteps,nx*ny*nz, ')'
!!!!-----------------------------------------------------------------------------         
       ALLOCATE ( jy(totsteps,nx,ny,nz), STAT = AllocateStatus)
       IF (AllocateStatus /= 0) STOP "*** Not enough memory FOR jy ***"
       write(*,*) 'AllocateStatus for jy=',AllocateStatus
       write(*,*)'(nx*ny*nz) = (',totsteps,nx*ny*nz, ')'
!!!!-----------------------------------------------------------------------------         
       ALLOCATE ( jz(totsteps,nx,ny,nz), STAT = AllocateStatus)
       IF (AllocateStatus /= 0) STOP "*** Not enough memory FOR jz ***"
       write(*,*) 'AllocateStatus for jz=',AllocateStatus
       write(*,*)'(nx*ny*nz) = (',totsteps,nx*ny*nz, ')'
!!!!-----------------------------------------------------------------------------

       !====================================================================================================
       
       do tstp=1,totsteps
          write(f2,'(I7.7,a)') start+(tstp*interval)-interval
          write(*,*) 'f2 is ', trim(f2)
          open(unit=tstp*10+14, file="output_iter/td."//trim(f2)//"/current-x.dx",status='old')
          open(unit=tstp*10+15, file="output_iter/td."//trim(f2)//"/current-y.dx",status='old')
          open(unit=tstp*10+16, file="output_iter/td."//trim(f2)//"/current-z.dx",status='old')
          !!!!---------------------------------------------         
          read(tstp*10+14,*) gbg,gbg,gbg,gbg,gbg,nx,ny,nz
          read(tstp*10+14,*) gbg,x0,y0,z0
          read(tstp*10+14,*) gbg, spacing
          read(tstp*10+14,*) gbg
          read(tstp*10+14,*) gbg
          read(tstp*10+14,*) gbg
          read(tstp*10+14,*) gbg          
          !!!!---------------------------------------------         
          read(tstp*10+15,*) gbg,gbg,gbg,gbg,gbg,nx,ny,nz
          read(tstp*10+15,*) gbg,x0,y0,z0
          read(tstp*10+15,*) gbg, spacing
          read(tstp*10+15,*) gbg
          read(tstp*10+15,*) gbg
          read(tstp*10+15,*) gbg
          read(tstp*10+15,*) gbg
          !!!!---------------------------------------------         
          read(tstp*10+16,*) gbg,gbg,gbg,gbg,gbg,nx,ny,nz
          read(tstp*10+16,*) gbg,x0,y0,z0
          read(tstp*10+16,*) gbg, spacing
          read(tstp*10+16,*) gbg
          read(tstp*10+16,*) gbg
          read(tstp*10+16,*) gbg
          read(tstp*10+16,*) gbg
          !!!!---------------------------------------------         
          do i=1,nx
             do j=1,ny
                do k=1,nz
                   read(tstp*10+14,*) jx(tstp,i,j,k)
                   read(tstp*10+15,*) jy(tstp,i,j,k)
                   read(tstp*10+16,*) jz(tstp,i,j,k)
                end do
             end do
          end do
          !!!!---------------------------------------------         
          close(tstp*10+14)
          close(tstp*10+15)
          close(tstp*10+16)          

          !----------------------------------- WRITING ----------------------
          open(unit=omega*10+7, file="output_iter/CURRENT/"//trim(f2)//"_current_at_z=0.dat" ,status='unknown')
          !write(*,*) nx, ny, nz
          k=int(nz/2.0)+1
          do i=1,nx
             do j=1,ny
                if ( i==1 .AND. j==1) then
                   write(omega*10+7,*) (i-(int(nx/2)+1))*spacing, (j-(int(nz/2)+1))*spacing, jx(tstp,i,j,k), jy(tstp,i,j,k)
                elseif ( MOD(i,reso)==0 .AND. MOD(j,reso)==0) then !!!! .AND.MOD(k,reso)==0 ) then
                   write(omega*10+7,*) (i-(int(nx/2)+1))*spacing, (j-(int(nz/2)+1))*spacing, jx(tstp,i,j,k), jy(tstp,i,j,k)
                elseif ( i==nx .AND. j==ny) then
                   write(omega*10+7,*) (i-(int(nx/2)+1))*spacing, (j-(int(nz/2)+1))*spacing, jx(tstp,i,j,k), jy(tstp,i,j,k)
                endif
             enddo
          end do
          close(omega*10+7)
          !------------------------------------------------------------------

       enddo
       



       
!!!!-----------------------------------------------------------------------------         
       ALLOCATE ( sumjx(nx,ny,nz), STAT = AllocateStatus)
       IF (AllocateStatus /= 0) STOP "*** Not enough memory FOR sumjx ***"
       write(*,*) 'AllocateStatus for sumjx=',AllocateStatus
       write(*,*)'(nx*ny*nz) = (',totsteps,nx*ny*nz, ')'
!!!!-----------------------------------------------------------------------------         
       ALLOCATE ( sumjy(nx,ny,nz), STAT = AllocateStatus)
       IF (AllocateStatus /= 0) STOP "*** Not enough memory FOR sumjy ***"
       write(*,*) 'AllocateStatus for sumjy=',AllocateStatus
       write(*,*)'(nx*ny*nz) = (',totsteps,nx*ny*nz, ')'
!!!!-----------------------------------------------------------------------------         
       ALLOCATE ( sumjz(nx,ny,nz), STAT = AllocateStatus)
       IF (AllocateStatus /= 0) STOP "*** Not enough memory FOR sumjz ***"
       write(*,*) 'AllocateStatus for sumjz=',AllocateStatus
       write(*,*)'(nx*ny*nz) = (',totsteps,nx*ny*nz, ')'
 !!!!-----------------------------------------------------------------------------

       do i=1,nx
          do j=1,ny
             do k=1,nz
                sumjx(i,j,k)=0.0
                sumjy(i,j,k)=0.0
                sumjz(i,j,k)=0.0
             end do
          end do
       end do
!!!!---------------------------------------------
       do tstp=1,totsteps
          do i=1,nx
             do j=1,ny
                do k=1,nz
                   sumjx(i,j,k) = sumjx(i,j,k) + jx(tstp,i,j,k)
                   !if ( i==(1*reso) .AND. j==(8*reso) .AND. k==(int(nz/2.0)+1) ) then
                   !   write(*,*) i, j, k, totsteps, jx(tstp,i,j,k), sumjx(i,j,k)
                   !endif
                   sumjy(i,j,k)=sumjy(i,j,k)+jx(tstp,i,j,k)
                   sumjz(i,j,k)=sumjz(i,j,k)+jx(tstp,i,j,k)
                end do
             end do
          end do
       enddo
!!!!-----------------------------------------------------------------------------

       open(unit=34, file="output_iter/CURRENT/average_from_"//trim(strstart)//"to"//trim(strend)//"current_at_z=0.dat" ,status='unknown')
       k=int(nz/2.0)+1
          do i=1,nx
             do j=1,ny
                if ( i==1 .AND. j==1) then
                   write(34,*) (i-(int(nx/2)+1))*spacing, (j-(int(nz/2)+1))*spacing, sumjx(i,j,k)/totsteps, sumjy(i,j,k)/totsteps !!!!!!!!, sumjz(i,j,k)/totsteps
                elseif ( MOD(i,reso)==0 .AND. MOD(j,reso)==0) then !!!! .AND.MOD(k,reso)==0 ) then
                   write(34,*) (i-(int(nx/2)+1))*spacing, (j-(int(nz/2)+1))*spacing, sumjx(i,j,k)/totsteps, sumjy(i,j,k)/totsteps !!!!!!!!, sumjz(i,j,k)/totsteps
                elseif ( i==nx .AND. j==ny) then
                   write(34,*) (i-(int(nx/2)+1))*spacing, (j-(int(nz/2)+1))*spacing, sumjx(i,j,k)/totsteps, sumjy(i,j,k)/totsteps !!!!!!!!, sumjz(i,j,k)/totsteps
                endif
             enddo
          end do
       close(34)

!!!!---------------------------------------------
       DEALLOCATE ( jx )
       DEALLOCATE ( jy )
       DEALLOCATE ( jz )
       DEALLOCATE (sumjx)
       DEALLOCATE (sumjy)
       DEALLOCATE (sumjz)
       
     end program induced_density


























































































