program rcrossj_int
  implicit none
  integer*4 now(3), today(3)
  double precision mx, my, mz, angstrom_in_au, jellcen_x, jellcen_y, jellcen_z 
  integer n_max,nx,ny,nz,tstp,max_tstp,tstp_intrvl,i,j,k,n,AllocateStatus,n_w,prmtr,omega,line
  integer xx, yy, zz, ilspr, iw1, iw2, iw3
  double precision spacing,dtm,tm(0:10000),tm_max, t_cut,w_los,w_max,AVG,dw
  character (len=90) :: flnm, slash, filename
  character(100) path, folder, folder1, folder2, folder3, makedirectory*40
  character(1000) f1, f2
  character*24 string
  double precision, DIMENSION(:, :, :), ALLOCATABLE :: rhogs, jx, jy, jz, rxj_x, rxj_y,rxj_z
  double precision, DIMENSION(:, :), ALLOCATABLE :: Sxy
  double precision, DIMENSION(:), ALLOCATABLE :: func, Sx, func_y

  integer, DIMENSION(:), ALLOCATABLE :: GEOM1
  double precision, DIMENSION(:), ALLOCATABLE :: CHRG1, GEOM1X, GEOM1Y, GEOM1Z
  integer :: NATOMS
  double precision x0, y0, z0, dx, dy, dz, zeors
  double precision autoarm
  CHARACTER(len=100) :: gbg, stepname


  angstrom_in_au = 1.889726 ! 1 Angstrom = 1.889726 atomic units

  open(21,file="input_rcrossj_int_octopus",status='old')
  write(*,*) ' '
  read(21,'(a)') f1
  write(*,*) 'The path for the output_iter/ is ', trim(f1)
  write(*,*) ' '
  read(21,*) jellcen_x, jellcen_y, jellcen_z
  write(*,*) 'Center of the structure : (x, y, z) =', jellcen_x, jellcen_y, jellcen_z
  write(*,*) ' '
   write(*,*) 'GOING FOR: '
  write(*,*) trim(f1),'output_iter/td.0000000/density.cube'
  write(*,*) ' '
  open(11,file=trim(f1)//"output_iter/td.0000000/density.cube",status='old')
 ! read(11,*) gbg,gbg,gbg,gbg,gbg,nx,ny,nz
 ! read(11,*) gbg,x0,y0,z0
 ! read(11,*) gbg, spacing
 ! read(11,*) gbg
 ! read(11,*) gbg
 ! read(11,*) gbg
 ! read(11,*) gbg
  read(11,*) gbg
  write(*,*) gbg
  read(11,*) gbg
  write(*,*) gbg
  read(11,*) NATOMS, x0,y0,z0
  write(*,*) NATOMS, x0,y0,z0
  read(11,*) nx, dx, zeors, zeors
  read(11,*) ny, zeors, dy, zeors
  read(11,*) nz, zeors, zeors, dz
  write(*,*) nx, dx, zeors, zeors
  write(*,*) ny, zeors, dy, zeors
  write(*,*) nz, zeors, zeors, dz

  ALLOCATE ( GEOM1(NATOMS), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
  ALLOCATE ( CHRG1(NATOMS), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
  ALLOCATE ( GEOM1X(NATOMS), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
  ALLOCATE ( GEOM1Y(NATOMS), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
  ALLOCATE ( GEOM1Z(NATOMS), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"

  DO i=1,NATOMS
     read(11,*) GEOM1(i), CHRG1(i), GEOM1X(i), GEOM1Y(i), GEOM1Z(i)
     write(*,'(I4,E13.5,3F13.5)') GEOM1(i), CHRG1(i), GEOM1X(i), GEOM1Y(i), GEOM1Z(i)
  ENDDO
  close(11)

  write(*,*) 'No. of points in each .cube (nx,ny,nz) = (',nx,ny,nz, ')'
  write(*,*) ' '
  write(*,*) 'Origin is set at (x0,y0,z0) atomic units'
  write(*,*) 'x0 =', x0
  write(*,*) 'y0 =', y0
  write(*,*) 'z0 =', z0
  write(*,*) 'xSpacing used', dx, '[in atomic units]' !=', dx/angstrom_in_au, 'Armstrong'
  write(*,*) 'ySpacing used', dy, '[in atomic units]' !=', dy/angstrom_in_au, 'Armstrong'
  write(*,*) 'zSpacing used', dz, '[in atomic units]' !=', dz/angstrom_in_au, 'Armstrong'
  write(*,*) ' '
  IF(dx==dy.AND.dy==dz)THEN
     spacing=dx
  ELSE
     write(*,*) ' '
     write(*,*) 'Spacings are different in x, y, and z directions!'
     write(*,*) 'This program will not work!'
     write(*,*) ' '
  ENDIF
!  dx = dx/angstrom_in_au
!  dy = dy/angstrom_in_au
!  dz = dz/angstrom_in_au

!  open(13,file=trim(f1)//"coords.txt",status='unknown')
!  ALLOCATE ( rhogs(nx, ny, nz), STAT = AllocateStatus)
!  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
!  do i=1,nx
!     do j=1,ny
!        do k=1,nz
!           read(11,*) rhogs(i,j,k)
!           write(13,*) (x0+(i-1)*spacing), (y0+(j-1)*spacing), (z0+(k-1)*spacing)
!        end do
!     end do
!  end do
!  close(11)
!  close(13)
  !---------------------------------------------------------------------------------------------
  !              Here we take all the input information we need to run this program
  !---------------------------------------------------------------------------------------------
  read(21,*) max_tstp
  write(*,*) 'The maximum nuber of time step to be considered = ', max_tstp
  read(21,*) tstp_intrvl
  write(*,*) 'The step interval used in octopus td run to generate td.0XXXXXX/ = ', tstp_intrvl
  prmtr=tstp_intrvl
  read(21,*) dtm
  write(*,*) 'TDTimeStep in eV-A units used in octopus td run = dt = ', dtm
  write(*,*) ' '
  n_max=max_tstp/tstp_intrvl
  write(*,*) 'The number of TD steps =',n_max
  

  do n=0,n_max
     tm(n)=n*dtm*prmtr                 ! 'i.e. we take time in atomic units'
  end do
  tm_max=tm(n_max)  !
  write(*,*) 'Total TIME in eV-A units used in octopus td run is',tm_max
  close(21)


  open(35,file=trim(f1)//"magmom_integrated-rxj_octopus.dat",status='unknown')
  write(35,*) "### tstp, tstp*tstp_intrvl*dtm, mx, my, mz !!!! Time in hbar/eV"
  !write(35,*) "### tstp, tstp*tstp_intrvl*dtm/27.212, mx, my, mz ! Time in a.u."

  do tstp=0,n_max
     !!!! write(*,*) tstp*prmtr
     write(flnm,'(I7.7,a)') tstp*prmtr
     write(*,*) 'We are now going for current-*.cube files in td.',flnm
     WRITE(slash,'(a)')"/"
 
     mx = 0.0
     my = 0.0
     mz = 0.0
     
     open(15,file=trim(f1)//"output_iter/td."//trim(flnm)//trim(slash)//"current-x.cube",status='old')
     open(16,file=trim(f1)//"output_iter/td."//trim(flnm)//trim(slash)//"current-y.cube",status='old')
     open(17,file=trim(f1)//"output_iter/td."//trim(flnm)//trim(slash)//"current-z.cube",status='old')

     ALLOCATE ( jx(nx, ny, nz), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
     ALLOCATE ( jy(nx, ny, nz), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
     ALLOCATE ( jz(nx, ny, nz), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
 
     ALLOCATE ( rxj_x(nx, ny, nz), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
     ALLOCATE ( rxj_y(nx, ny, nz), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
     ALLOCATE ( rxj_z(nx, ny, nz), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
     do line=1,NATOMS+6
        read(15,*) gbg
        read(16,*) gbg
        read(17,*) gbg
     enddo
     do i=1,nx
        do j=1,ny
           !!do k=1,nz
              read(15,*) ( jx(i,j,k), k=1,nz )
              read(16,*) ( jy(i,j,k), k=1,nz )
              read(17,*) ( jz(i,j,k), k=1,nz )
           !!end do
        end do
     end do
     do i=1,nx
        do j=1,ny
           do k=1,nz
              rxj_x(i,j,k) = (y0+(j-1)*spacing)*jz(i,j,k) - (z0+(k-1)*spacing)*jy(i,j,k)
              rxj_y(i,j,k) = (z0+(k-1)*spacing)*jx(i,j,k) - (x0+(i-1)*spacing)*jz(i,j,k)
              rxj_z(i,j,k) = (x0+(i-1)*spacing)*jy(i,j,k) - (y0+(j-1)*spacing)*jx(i,j,k)
           end do
        end do
     end do
     close(15)
     close(16)
     close(17)

!!!!!!MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM     
!!!!!!             Writing rXj in cube format
!!!!!!MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM     
!
!     open(18,file=trim(f1)//"output_iter/td."//trim(flnm)//trim(slash)//"rxj_x.cube",status='unknown')
!     open(19,file=trim(f1)//"output_iter/td."//trim(flnm)//trim(slash)//"rxj_y.cube",status='unknown')
!     open(20,file=trim(f1)//"output_iter/td."//trim(flnm)//trim(slash)//"rxj_z.cube",status='unknown')
!     write(18,*) 'Generated by magjellcode'
!     write(18,*) 'Generated by magjellcode'
!     write(18,'(I4,3E13.5)') NATOMS, x0,y0,z0
!     write(18,'(I4,3E13.5)') nx, dx, zeors, zeors
!     write(18,'(I4,3E13.5)') ny, zeors, dy, zeors
!     write(18,'(I4,3E13.5)') nz, zeors, zeors, dz
!     DO i=1,NATOMS
!        write(18,'(I4,E13.5,3E13.5)') GEOM1(i), CHRG1(i), GEOM1X(i), GEOM1Y(i), GEOM1Z(i)
!     ENDDO
!     write(19,*) 'Generated by magjellcode'
!     write(19,*) 'Generated by magjellcode'
!     write(19,'(I4,3E13.5)') NATOMS, x0, y0, z0
!     write(19,'(I4,3E13.5)') nx, dx, zeors, zeors
!     write(19,'(I4,3E13.5)') ny, zeors, dy, zeors
!     write(19,'(I4,3E13.5)') nz, zeors, zeors, dz
!     DO i=1,NATOMS
!        write(19,'(I4,E13.5,3E13.5)') GEOM1(i), CHRG1(i), GEOM1X(i), GEOM1Y(i), GEOM1Z(i)
!     ENDDO
!     write(20,*) 'Generated by magjellcode'
!     write(20,*) 'Generated by magjellcode'
!     write(20,'(I4,3E13.5)') NATOMS, x0,y0,z0
!     write(20,'(I4,3E13.5)') nx, dx, zeors, zeors
!     write(20,'(I4,3E13.5)') ny, zeors, dy, zeors
!     write(20,'(I4,3E13.5)') nz, zeors, zeors, dz
!     DO i=1,NATOMS
!        write(20,'(I4,E13.5,3E13.5)') GEOM1(i), CHRG1(i), GEOM1X(i), GEOM1Y(i), GEOM1Z(i)
!     ENDDO
!     do i=1,nx
!        do j=1,ny
!           !do k=1,nz
!              write(18,'(6E13.5)') (rxj_x(i,j,k),k=1,nz)
!              write(19,'(6E13.5)') (rxj_y(i,j,k),k=1,nz)
!              write(20,'(6E13.5)') (rxj_z(i,j,k),k=1,nz)
!           !end do
!        end do
!     end do
!     close(18)
!     close(19)
!     close(20)
!
!!!!!!MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM     

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!        
     ALLOCATE ( func(nz), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
     ALLOCATE ( Sxy(nx, ny), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
     ALLOCATE ( func_y(ny), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
     ALLOCATE ( Sx(nx), STAT = AllocateStatus)
     IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"     
     call VOL_INTEGRATION(rxj_x,Sxy,func,func_y,Sx,nx,ny,nz,mx,spacing)
     call VOL_INTEGRATION(rxj_y,Sxy,func,func_y,Sx,nx,ny,nz,my,spacing)
     call VOL_INTEGRATION(rxj_z,Sxy,func,func_y,Sx,nx,ny,nz,mz,spacing)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     IF( ALLOCATED(func) ) DEALLOCATE (func)
     IF( ALLOCATED(func_y) ) DEALLOCATE (func_y)
     IF( ALLOCATED(Sxy) ) DEALLOCATE (Sxy)
     IF( ALLOCATED(Sx) ) DEALLOCATE (Sx)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     
     !write(*,*) mx
     !write(*,*) my
     !write(*,*) mz
     
     write(35,*) tstp, tstp*tstp_intrvl*dtm, mx, my, mz ! Time in hbar/eV
     !write(35,*) tstp, tstp*tstp_intrvl*dtm/27.212, mx, my, mz ! Time in a.u.
     
     IF( ALLOCATED(jx) ) DEALLOCATE (jx)
     IF( ALLOCATED(jy) ) DEALLOCATE (jy)
     IF( ALLOCATED(jz) ) DEALLOCATE (jz)
     IF( ALLOCATED(rxj_x) ) DEALLOCATE (rxj_x)
     IF( ALLOCATED(rxj_y) ) DEALLOCATE (rxj_y)
     IF( ALLOCATED(rxj_z) ) DEALLOCATE (rxj_z)
     
  enddo
  close(35)
    
end program rcrossj_int




!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

Subroutine trapizoidal(F,integral,n,deltax)
  double precision integral
  double precision deltax
  double precision, DIMENSION(n) :: F
  integer n, i
  integral = F(1)+F(n)
  do i=2, n-2, 1
     integral = integral + 2*F(i)
  end do
  integral = integral*0.5*deltax
return
end subroutine trapizoidal

!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

Subroutine VOL_INTEGRATION(integrand,Sxy,func,func_y,Sx,nx,ny,nz,integral,grid_spacing)
  
  double precision,  DIMENSION(nx,ny,nz) :: integrand
  double precision,  DIMENSION(nx,ny) :: Sxy
  double precision,  DIMENSION(nz) :: func
  double precision,  DIMENSION(ny) :: func_y
  double precision,  DIMENSION(nx) :: Sx
  integer i,j,k,nx,ny,nz
  double precision integral, justsum
  double precision grid_spacing

!  justsum = 0.d0
!  do i=1,nx
!     do j=1,ny
!        do k=1,nz
!           justsum = justsum + integrand(i,j,k)
!        enddo
!     end do
!  end do
!  write(*,*) justsum

  do i=1,nx
     do j=1,ny
        do k=1,nz
           func(k) = integrand(i,j,k)   !!!! The data in 3d grid enters here.
        enddo
        call trapizoidal(func,integral,nz,grid_spacing)
        Sxy(i,j) = integral
        !write(*,*) i, j, Sxy(i,j)
     end do
  end do  
  !!=========================================================================================   
  !! If you want to see S(x,y), the z-integrated integrand(x,y,z) can be written at this step 
  !!=========================================================================================   

  do i=1,nx
     do j=1,ny
        func_y(j) = Sxy(i,j)
     end do
     call trapizoidal(func_y,integral,ny,grid_spacing)
     Sx(i) = integral
     !write(*,*) i, Sx(i)
  end do  
  !!=============================================================================   
  !! If you want to see S(x), the y-integrated S(x,y) can be written at this step
  !!=============================================================================  
  
  call trapizoidal(Sx,integral,nx,grid_spacing)
  return
  
end subroutine VOL_INTEGRATION
      
!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!!!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@       
